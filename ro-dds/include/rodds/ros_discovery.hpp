#pragma once
#include <iostream>
#include <string>

#define ROS_DISCOVERY_TOPIC_NAME "ros_discovery_info"
#define ROS_DISCOVERY_TOPIC_TYPE "rmw_dds_common::msg::dds_::ParticipantEntitiesInfo_"

extern "C"
{
    #include "cdds/cdds_util.h"
}

namespace ros_discovery
{

class RosDiscoveryManager
{
public: 
    RosDiscoveryManager(const dds_entity_t& participant)
    {
        auto topic_name = const_cast<char*>(ROS_DISCOVERY_TOPIC_NAME);
        auto topic_type = const_cast<char*>(ROS_DISCOVERY_TOPIC_TYPE);
        auto topic = cdds_create_blob_topic(
            participant, topic_name, topic_type, true);
        
        dds_qos_t *qos = dds_create_qos();
        qos->reliability = {
            .kind = dds_reliability_kind::DDS_RELIABILITY_RELIABLE, 
            .max_blocking_time = DDS_INFINITY
        };
        qos->durability = {
            .kind = dds_durability_kind::DDS_DURABILITY_TRANSIENT_LOCAL
        };
        qos->history = {
            .kind = dds_history_kind::DDS_HISTORY_KEEP_ALL,
            .depth = 0
        };
        qos->ignorelocal= {
            .value = dds_ignorelocal_kind::DDS_IGNORELOCAL_PARTICIPANT
        };
        
        auto reader = dds_create_reader(participant, topic, qos, nullptr);
        if (reader < 0)
        {
            //TODO implement exception or log error;
        }
    };

    void drop()
    {
        //TODO add result check
        dds_return_t result = 0;
        result = dds_delete(this->reader);
        result = dds_delete(this->writer);
    };

    dds_entity_t* get_participant()
    {
        return &this->participant;
    };

    dds_entity_t* get_reader()
    {
        return &this->reader;
    };

    dds_entity_t* get_writer()
    {
        return &this->writer;
    };

private:
    dds_entity_t participant;
    dds_entity_t reader;
    dds_entity_t writer;
};

void print(const std::string& value);
}
