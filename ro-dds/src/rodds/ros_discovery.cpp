#include <iostream>
// extern "C" 
// {
// #include "cdds/cdds_builtin.h"
// }

#include "rodds/ros_discovery.hpp"

namespace ros_discovery
{
    void print(const std::string& value)
    {
        std::cout << value << std::endl;
    }
}