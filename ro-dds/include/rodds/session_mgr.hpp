#pragma once
#include <iostream>
#include <vector>
#include <zmq.hpp>
#include <zmq_addon.hpp>

namespace rodds::session_mgr
{
// addresses wich topic pub/sub
const std::string READER_ADDRESS = "/tmp/reader.ipc";
const std::string WRITER_ADDRESS = "/tmp/writer.ipc";

// global topics namespace
const std::string TOPIC_NAMESPACE = "/@rodds";

// forward pub/sub topic names
const std::string FWD_PUBLICATION_TOPIC = "/fwd_pub";
const std::string FWD_SUBSCRIPTION_TOPIC = "/fwd_sub";
const std::string FWD_DISCOVERY_TOPIC = "/fwd_disco";

// local pub/sub topic names
const std::string LOC_PUBLICATION_TOPIC = "/loc_pub";
const std::string LOC_SUBSCRIPTION_TOPIC = "/loc_sub";
const std::string LOC_DISCOVERY_TOPIC = "/loc_disco";


class SessionMgr
{
public:
    SessionMgr(const unsigned int context_id);

    // pub payload data by key
    void pub_topic_payload(const std::string& key, 
                           const std::vector<unsigned char>& payload);

    // pub discovery event by key
    void pub_discovery_event(const std::string& key,
                             const std::string& event);
private:
    zmq::context_t _ctx;
    zmq::socket_t _pub;
    zmq::socket_t _sub;
};
} // namespace rodds::session_mgr
