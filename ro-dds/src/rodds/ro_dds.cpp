#include "rodds/dds_discovery.hpp"
#include <istream>
#include <iterator>
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_DEBUG
#include <spdlog/spdlog.h>
#include "rodds/ro_dds.hpp"

namespace rodds
{
RoDdsManager::RoDdsManager(boost::asio::io_service& ios, 
                 boost::asio::local::stream_protocol::socket& tx,
                 boost::asio::local::stream_protocol::socket& rx):
                 _loc_ios(ios),_loc_snd(tx), _loc_rcv(rx), _session(1)
{
    SPDLOG_INFO("RoDdsManager initialize");
}

void RoDdsManager::run()
{
    // initialize domain participant
    _dp = dds_create_participant(0, NULL, NULL);

    // start local discovery
    run_local_discovery();

    // start discovery
    dds_discovery::run_discovery(_dp, &_loc_snd);   

    // start io_service (reader/writer events)
    _loc_ios.run();
}
void RoDdsManager::read_local_event()
{
    SPDLOG_DEBUG("Read local event");
    boost::asio::async_read_until(_loc_rcv, _loc_buf, " ",
        boost::bind(
            &RoDdsManager::on_read_local_event_length,
            this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred
        )
    );
}
    
void RoDdsManager::on_read_local_event_length(const boost::system::error_code &error,
                                              std::size_t bytes_transdered)
{
    std::size_t length;
    char space;
    if (!error.failed() &&
        std::istream(&_loc_buf) >> std::noskipws >> length >> space && space == ' ')
    {
        if (length <= _loc_buf.size())
            on_read_local_event(error, 0);
        else
            boost::asio::async_read(_loc_rcv, _loc_buf,
                boost::asio::transfer_exactly(length - _loc_buf.size()),
                boost::bind(
                    &RoDdsManager::on_read_local_event,
                    this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred
                )
            );
    }
}
    
void RoDdsManager::on_read_local_event(const boost::system::error_code &error,
                             std::size_t bytes_transferd)
{
    std::istream msg(&_loc_buf);
    auto event = dds_discovery::restore_from_wire<dds_discovery::DiscoveryEvent>(msg);
    //TODO: implement publication of discovered events

    if (event.event_type == dds_discovery::DiscoveryEvent::DiscoveredPublication)
    {
        SPDLOG_DEBUG("Catch discovery event: {0}\n\tkey: {1}\n\ttopic_name: {2}\n\ttopic_type: {3}",
            "DiscoveredPublication",
            event.entity->key.c_str(),
            event.entity->topic_name.c_str(),
            event.entity->topic_type.c_str()
        );
        
        _session.pub_discovery_event(event.entity->key, rodds::dds_discovery::get_wire_format(event));
    } else if (event.event_type == dds_discovery::DiscoveryEvent::DiscoveredSubscription) {
        SPDLOG_DEBUG("Catch discovery event: {0}\n\tkey: {1}\n\ttopic_name: {2}\n\ttopic_type: {3}",
            "DiscoveredSubscription",
            event.entity->topic_name.c_str(),
            event.entity->topic_type.c_str(),
            event.entity->key.c_str()
        );

        _session.pub_discovery_event(event.entity->key, rodds::dds_discovery::get_wire_format(event));
    } else if (event.event_type == dds_discovery::DiscoveryEvent::UndiscoveredPublication) {
        SPDLOG_DEBUG("Catch discovery event: {0}\n\tkey: {1}",
            "UndiscoveredPublication",
            event.entity->key.c_str()
        );

        _session.pub_discovery_event(event.entity->key, rodds::dds_discovery::get_wire_format(event));
    } else if (event.event_type == dds_discovery::DiscoveryEvent::UndiscoveredSubscription) {
        SPDLOG_DEBUG("Catch discovery event: {0}\n\tkey: {1}",
            "UndiscoveredSubscription",
            event.entity->key.c_str()
        );

        _session.pub_discovery_event(event.entity->key, rodds::dds_discovery::get_wire_format(event));
    }

    if (!error)
        read_local_event();
}


void RoDdsManager::run_local_discovery()
{    
    read_local_event(); 
}
} // namespace rodds
