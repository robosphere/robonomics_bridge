#pragma once

#include <boost/thread/pthread/mutex.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <dds/dds.h>
#include <memory>
#include <string>

#include <boost/asio.hpp>
#include <boost/thread.hpp>

extern "C"
{
#include "cdds/cdds_util.h"
#include "cdds/cdds_builtin.h"
}


#include "rodds/session_mgr.hpp"

#define MAX_SAMPLES 32
#define DDS_100MS_DURATION 100 * 1000000

namespace rodds::dds_discovery 
{

enum RouteStatus
{
    // entity passed via bridge and transfer data
    ROUTED,
    // error to upload entity via bridge
    NOT_ALLOWED, 
    // error to create entity mirror on opposing side
    CREATION_FAILED,
    // error to create with qos conflict
    QOS_CONFLICT, 
};

struct DDSEntity
{
    std::string key;
    std::string participant_key;
    std::string topic_name;
    std::string topic_type;
    bool keyless;
    // TODO uncomment when realize
    //dds_qos_t qos;
    // store routes of current dds_entity into hash_map with routes key`s
    //std::map<std::string, RouteStatus> routes;    

    template<typename Ar> 
    void serialize(Ar& ar, unsigned)
    {
        ar & key & participant_key & topic_name & topic_type & keyless;
    }
};

struct DiscoveryEvent
{   
    enum DiscoveryEventType
    {
        // discovered publication request for DDSEntit
        DiscoveredPublication,
        // undiscovered publication request for DDSEntit
        UndiscoveredPublication,
        // discovered subscription request for DDSEntity
        DiscoveredSubscription,
        // undiscovered subscription request for DDSEntity
        UndiscoveredSubscription 
    };

    std::shared_ptr<DDSEntity> entity;
    DiscoveryEventType event_type;
   
    template<typename Ar> 
    void serialize(Ar& ar, unsigned)
    {
        ar & entity & event_type;
    }
};

// serialization of package
template<typename T>
std::string get_wire_format(T const& t)
{
    std::ostringstream oss;
    {
        boost::archive::text_oarchive oa(oss);
        oa << t;
    }

    std::string data = std::move(oss).str();
    return std::to_string(data.length()) + " " + data;
}

// deserialization of package
template<typename T>
T restore_from_wire(std::istream &stream)
{
    T packet;
    boost::archive::text_iarchive ia(stream);
    ia >> packet;
    stream.ignore(2, '\n');
    return packet;
}

// callback function that bind with dds_discovery socket subscriber
void on_data(const dds_entity_t dr, void* arg);

// send discovery event if state of dds topics are changed
void send_discovery_event(const dds_entity_t dp, boost::asio::local::stream_protocol::socket *socket,
    const DiscoveryEvent& event);

// run dds discovery of all local topics active in dds namespace
void run_discovery(const dds_entity_t dp, boost::asio::local::stream_protocol::socket *socket);

// create dds writer to topic
dds_entity_t create_forwading_dds_writer(const dds_entity_t &dp, const std::string &topic_name,
    const std::string &topic_type, const dds_qos_t &qos, const bool keyless);

// create dds topic listener && bind incomming data with session writer
void data_forwader_listener(const dds_entity_t dr, void* arg);

// create dds reader for some period 
dds_entity_t create_forwading_dds_reader(const dds_entity_t &dp, const std::string &topic_name,
    const std::string &topic_type, const std::string &key, const dds_qos_t &qos, const bool keyless, 
    const std::chrono::system_clock::time_point &read_period, rodds::session_mgr::SessionMgr* session);
}

