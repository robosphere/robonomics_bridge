#include <iostream>
#include <string>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <string>
#include <execution>
#include <cstdint>

extern "C"
{

#include "cdds/cdds_util.h"

}

std::string ROS_DISCOVERY_TOPIC_NAME = "ros_discovery_info";
std::string ROS_DISCOVERY_TOPIC_TYPE = "rmw_dds_common::msg::dds_::ParticipantEntitiesInfo_";

int main()
{
    auto participant = dds_create_participant(0, nullptr, nullptr);
    auto topic_name = const_cast<char*>(ROS_DISCOVERY_TOPIC_NAME.c_str());
    auto topic_type = const_cast<char*>(ROS_DISCOVERY_TOPIC_TYPE.c_str());
    auto topic = cdds_create_blob_topic(
        participant, topic_name, topic_type, true);
    
    dds_qos_t *qos = dds_create_qos();
    qos->reliability = {
        .kind = dds_reliability_kind::DDS_RELIABILITY_RELIABLE, 
        .max_blocking_time = DDS_INFINITY
    };
    qos->durability = {
        .kind = dds_durability_kind::DDS_DURABILITY_TRANSIENT_LOCAL
    };
    qos->history = {
        .kind = dds_history_kind::DDS_HISTORY_KEEP_ALL,
        .depth = 0
    };
    qos->ignorelocal= {
        .value = dds_ignorelocal_kind::DDS_IGNORELOCAL_PARTICIPANT
    };
    
    auto reader = dds_create_reader(participant, topic, qos, nullptr);
    //dds_qos_delete(qos);
    if (reader < 0)
    {
        //TODO implement exception or log error;
    }

    std::cout << "reader init" << std::endl;

    // qos = dds_create_qos();
    // qos->reliability = {
    //     .kind = dds_reliability_kind::DDS_RELIABILITY_RELIABLE, 
    //     .max_blocking_time = DDS_INFINITY
    // };
    // qos->durability = {
    //     .kind = dds_durability_kind::DDS_DURABILITY_TRANSIENT_LOCAL
    // };
    // qos->history = {
    //     .kind = dds_history_kind::DDS_HISTORY_KEEP_LAST,
    //     .depth = 1
    // };
    // qos->ignorelocal= {
    //     .value = dds_ignorelocal_kind::DDS_IGNORELOCAL_PARTICIPANT
    // };
    // auto writer = dds_create_writer(participant, topic, qos, NULL);
    // //dds_qos_delete(qos);
    // if (writer < 0)
    // {
    //     //TODO implement exception or log error;
    // }

    // std::cout << "writer init" << std::endl;
 
    struct cdds_ddsi_payload *zp = nullptr;
    dds_sample_info_t sample_info;

    while(true)
    {
        if (cdds_take_blob(reader, &zp, &sample_info))
        {
            std::cout << "Payload:" << (int)zp->size << std::endl;
            std::stringstream ss;
            ss << std::hex;
            for (int i = 4; i < 20; ++i) {
                ss << std::setfill('0') << std::setw(2) << (int)zp->payload[i];
            }
            std::cout << "gid: " << ss.str() << std::endl;
            std::stringstream st;
            st << std::hex;
            for (int i = 0; i < (int)zp->size; ++i) {
                st << std::setfill('0') << std::setw(2) << (int)zp->payload[i];
            }
            std::cout << "payload: " << st.str() << std::endl;
            std::cout << "---------------------------------------" << std::endl;
        }
    }

    return 0;
}