#include <iostream>
#include <string>

extern "C"
{

#include "cdds/cdds_util.h"

}

int main(int argc, char *argv[])
{
    if (argc < 6) 
    {
        std::cout << "USAGE:\n\tbridge"
        << "<in-topic-name> <out-topic-name> <type_name>" 
        << "<in-topic-domain> <out-topic-domain> <keyless>" 
        << std::endl;
        return 1;
    }

    int keyless = atoi(argv[6]);
    dds_domainid_t d1 = atoi(argv[4]);
    dds_domainid_t d2 = atoi(argv[5]); 
    std::cout << "keyless: " << keyless << std::endl;

    const dds_entity_t in_dp = dds_create_participant(d1, NULL, NULL);
    const dds_entity_t out_dp = dds_create_participant(d2, NULL, NULL);
    
    struct ddsi_sertopic *in_sertopic = cdds_create_blob_sertopic(
                                        in_dp, argv[1], argv[3], keyless == 1);
    struct ddsi_sertopic *out_sertopic = cdds_create_blob_sertopic(
                                        out_dp, argv[2], argv[3], keyless == 1);
    
    const dds_entity_t in_topic = dds_create_topic_generic(
                                    in_dp, &in_sertopic, 0, 0, 0);
    const dds_entity_t out_topic = dds_create_topic_generic(
                                    out_dp, &out_sertopic, 0, 0, 0);

    dds_qos_t *qos = NULL;

    const dds_entity_t reader = dds_create_reader(
                                in_dp, in_topic, qos, NULL);
    const dds_entity_t writer = dds_create_writer(
                                out_dp, out_topic, qos, NULL);
    
    dds_return_t rc;
    while (true)
    {
        struct cdds_ddsi_payload *zp = NULL;
        dds_sample_info_t sample_info;
        rc = cdds_take_blob(reader, &zp, &sample_info);
        if (rc > 0)
        {
            std::cout << "Payload:" << (int)zp->size << std::endl;
            for (int i = 0; i < zp->size; ++i) {
                std::cout << (int)zp->payload[i] << " ";
            }
            std::cout << std::endl;
            std::cout << "trying to write ..." << std::endl;

            struct cdds_ddsi_payload *fwd = cdds_ddsi_payload_create(
                out_sertopic, zp->kind, zp->payload, zp->size);
            dds_writecdr(writer, (struct ddsi_serdata*)fwd);
        }
        dds_sleepfor(DDS_MSECS (1000));
    }

    dds_delete (in_dp);
    dds_delete (out_dp);

    return 0;
}
