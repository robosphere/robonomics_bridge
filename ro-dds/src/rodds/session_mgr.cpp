#include <zmq.hpp>
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_DEBUG

#include <spdlog/spdlog.h>
#include "rodds/session_mgr.hpp"
#include <cstring>

namespace rodds::session_mgr
{

SessionMgr::SessionMgr(const unsigned int context_id)
: _ctx(context_id), _pub(_ctx, zmq::socket_type::pub), _sub(_ctx, zmq::socket_type::sub)
{
    SPDLOG_DEBUG("SessionMgr initialize with context:({0})", context_id);
    std::string padd = "ipc://" + WRITER_ADDRESS;
    _pub.bind(padd);

    std::string sadd = "ipc://" + READER_ADDRESS;
    _sub.connect(sadd);
}

//TODO: test method
void SessionMgr::pub_topic_payload(const std::string& key, 
                                   const std::vector<unsigned char>& payload)
{
    auto ftopic = TOPIC_NAMESPACE + LOC_PUBLICATION_TOPIC + "/" + key;
    zmq::message_t topic((void*)ftopic.c_str(), ftopic.length(), NULL);
    zmq::message_t msg(payload.size());
    std::memcpy(msg.data(), payload.data(), payload.size());

    // send message to scepific topic
    _pub.send(topic, zmq::send_flags::sndmore);
    _pub.send(msg, zmq::send_flags::none);
}

//TODO: test method
void SessionMgr::pub_discovery_event(const std::string &key, const std::string &event)
{
    auto ftopic = TOPIC_NAMESPACE + LOC_DISCOVERY_TOPIC;
    zmq::message_t topic((void*)ftopic.c_str(), ftopic.length(), NULL); 
    // send topic name as filter for client
    _pub.send(topic, zmq::send_flags::sndmore);

    const std::string_view some(event);
    // send message to specific topic
    _pub.send(zmq::buffer(some, some.size()));

    SPDLOG_DEBUG("Send discovery event to: {0}", ftopic);
    std::cout << key << '\n' << event << std::endl;}

} // namespace rodds::session_mgr
